# Orange Book ETL Script
## Dependencies
* [Python 3.5+](https://www.python.org/downloads/)
* [Oracle Instant Client](http://www.oracle.com/technetwork/database/database-technologies/instant-client/overview/index.html)

### Python Packages
* cx_Oracle
* requests

## Installation
1. Clone repository to installation directory.
2. Create data dir.
3. Run pip install –r requirements.txt
4. Set data dir in OB-config.ini
5. Run OBLoad.bat in installed directory when ready.

Optional: Configure scheduled task to run OBLoad.bat on a regular basis.

Note: If populating a new database, may want to SET ADD_DT to NULL after first month’s load.

Note: If loading to existing database, be sure

## Configuration
### OB-config.ini
In this file, one can set various parameters for this script. They are as follows:
#### obdata
* url - Static URL of location of monthly OB data zip
* data - absolute path of folder in which to save zip and its contents

#### oracle
* user
* pass
* server
* port
* schema

#### tables
The table name for each table can be set here if it were to ever change in Oracle

## Logging
Logging settings are contained in the logging.ini file. Generally this should not need to be changed. Log file names are set here. 
The load process generates two logs.

* obload.log - a succinct log file noting when each table is refreshed and how many rows were added to it, as well as any exceptions.
* historylog.log - a detailed record of each row added/changed in each history table for debugging purposes

## Main Script Specs
Launching main.py will trigger first an attempt to retrieve the latest .zip from OB at the URL speficied in the ob-config.ini file. If the file exists, and it is newer than the last file in the data folder locally, the retrieval function will download the file, and unzip its contents to the data folder. It will then pass the name of this new file to the DMP load scripts. The dump load scripts will run, followed immediately by the history load scripts. It is possible to comment out a dump or history load function in main.py, if ever required. Specs for each load function are captured below.

## DUMP Load Specs
* Remove leading zeroes from PRODUCT_NO, APPL_NO
* Escape ' character
* Empty values set to Null
* Load entire CSV otherwise unmanipulated to DMP table setting LOAD_DT to last_update_date (set to the date of the load)

## T_OB_PRODUCT_DMP Specs
* Split DF;ROUTE on ";" into DOSAGE_FORM and ROUTE
* IF APPROVAL_DT is "Approved Prior to Jan 1, 1981", then APPROVAL_DT IS NULL and text added to REMARKS
* IF STRENGTH contains "**", split on that and add text to REMARKS

## Products History Load Specs
Load process logic:

1. Select every row from the T_OB_PRODUCTS_DMP table with a LOAD_DT that matches last_update_date (passed to function, not to be confused with LAST_UPDATE_DT column)
2. For each row in SQL query results, select any rows in T_OB_PRODUCTS_HISTORY where APPL_NO, TYPE, PRODUCT_NO, INGREDIENT match the value of the T_OB_PRODUCTS_DMP table row.
    1. If no match, create new record.
    2. If a single match, iterate through each column, confirm the value of the two records for that cell match.
        1. If a row does not match, delete the T_OB_PRODUCTS_HISTORY record and replace it with the new information and set the LAST_UPDATE_DT to  last_update_date.
    3. If there are >1 results, throw ambiguous match error.
3. After adding and modifying all rows, check for rows not present in the T_OB_PRODUCTS_DMP table that exist in the T_OB_PRODUCTS_HISTORY table. Set the result rows' REMOVE_DT to the last_update_date.

## Exclusivity History Load Specs
Load process logic:

1.	Each month build a temp table grouping by APPL_TYPE, APPL_NO, PRODUCT_NO, EXCLUSIVITY_CODE, EXCLUSIVITY_DATE. Produce a count of identical rows for an "EXCLUSIVITY_COUNT"
2.	Check the existing history table,
    1. if the count is off, delete/re-add the row with the new count, update LAST_UPDATE_DT to last_update_date
    2. Add rows in the DMP table that do not match on APPL_TYPE, APPL_NO, PRODUCT_NO, EXCLUSIVITY_CODE, EXCLUSIVITY_DATE
    3. After adding and modifying all rows, check for rows not present in the T_OB_EXCLUSIVITY_DMP table that do exist in the T_OB_EXCLUSIVITY_HISTORY table, if the REMOVE_DT is null, set it to the current last_update_date