import zipfile
import requests
import logging
import logging.config
import re
import os.path



def unzip(logger, zipname, source, dest):
    try:
        zip_ref = zipfile.ZipFile(source)
        dest = os.path.join(dest, zipname.replace('.zip', ''))
        zip_ref.extractall(dest)
        zip_ref.close()
        return dest
    except zipfile.BadZipfile as e:
        logger.error('Bad zip' + zipname)

# Retrieval script
def retrieveob(logger, data, url):
    failed = False
    try:
        r = requests.get(url)
        r.raise_for_status()
    except requests.exceptions.ConnectionError as e:
        logger.error('Connection Error - ' + url)
    except requests.exceptions.HTTPError as e:
        if e.response.status_code == 404:
            logger.error('Page not found - ' + url)
        elif e.response.status_code == 503:
            logger.error('503 Server Error - ' + url)
        else:
            print(e.response)
        failed = True

    if not failed:
        d = r.headers['content-disposition']
        fname = re.findall("filename=\"(.+)\"", d)[0]
        if os.path.isfile(os.path.join(data, 'zips', fname)):
            logger.info('Previously retrieved data: ' + fname.replace('.zip', '') + '. Will not load.')
            return None
        else:
            with open(os.path.join(data, 'zips', fname), 'wb') as output:
                output.write(r.content)
            dir = unzip(logger, fname, os.path.join(data, 'zips', fname), data)
            logger.info('Successful extract: ' + fname.replace('.zip', ''))
            return dir

    else:
        logger.error('Retrieval failed')
        return None

if __name__ == "__main__":
    url = 'https://www.fda.gov/downloads/Drugs/InformationOnDrugs/UCM163762.zip'
    # logging instantiation
    logging.config.fileConfig('logging.ini')
    # create logger
    logger = logging.getLogger('OBRetrieve')
    data = 'C:/Users/cep/Documents/PycharmProjects/OBLoad/data'
    retrieveob(logger, data, url)
