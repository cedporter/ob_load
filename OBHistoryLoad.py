###
# Dependency on Oracle Instant Client
###

import logging
import logging.config
import cx_Oracle
from helper import formatDate


def loadProductsHistory(logger, cur, con, tables, last_update_date):
    logger.info('Beginning Product History table load.')
    # Read products CSV and iterate through rows
    logger.info('Retrieving Product Dump Table')
    select = """SELECT *
                FROM {0} T
                WHERE T.LOAD_DT = TO_DATE('{1}', 'dd-mon-yyyy')"""
    cur.execute(select.format(tables['productsdump'], last_update_date))
    results = cur.fetchall()
    datload = []

    logger.info('Beginning Product table load.')
    for result in results:
        action = 'none'
        select = """SELECT *
                        FROM {0}
                        WHERE APPL_NO = '{1}'
                            AND TYPE = '{2}'
                            AND PRODUCT_NO = '{3}'
                            AND INGREDIENT = '{4}'"""
        cur.execute(select.format(tables['productshistory'], result[6], result[12], result[7], result[0]))
        qresults = cur.fetchall()
        count = len(qresults)
        # There are no matching records in the DB, add a new record for this row
        if count == 0:
            logger.debug('New record: ' + str(result))
            action = 'add'
        # There are duplicate records. Strange. Do nothing. Investigate.
        elif count > 1:
            logger.debug('Ambiguous matches in Product History Table: ' + str(result))
            c = 1
            for r in qresults:
                logger.debug('Ambiguous match ' + str(c) + ': ' + str(r))
                c += 1
        elif count == 1:
            existingrecord = qresults[0]
            # If the record is identical to the existingrecord in the csv, preserve, no update.
            pmatch = True
            i = 0
            while i <= 15:
                if result[i] != existingrecord[i]:
                    logger.debug('Imperfect product history match: {0} != {1}'.format(result[i], existingrecord[i]))
                    pmatch = False
                i += 1
            if not pmatch:
                logger.debug('Imperfect match product dmp record: ' + str(result))
                logger.debug('Imperfect match product history record, drop/readd: ' + str(existingrecord))
                action = 'change'
                delete = """DELETE FROM {0}
                                WHERE APPL_NO = '{1}'
                                AND TYPE = '{2}'
                                AND PRODUCT_NO = '{3}'
                                AND INGREDIENT = '{4}'"""
                cur.execute(delete.format(tables['productshistory'],
                                          existingrecord[6],
                                          existingrecord[12],
                                          existingrecord[7],
                                          existingrecord[0]))
                con.commit()

        if action == 'add':
            datload.append((result[0], #1
                            result[1], #2
                            result[2], #3
                            result[3], #4
                            result[4], #5
                            result[5], #6
                            result[6], #7
                            result[7], #8
                            result[8], #9
                            formatDate(result[9]), #10
                            result[10], #11
                            result[11], #12
                            result[12], #13
                            result[13], #14
                            result[14], #15
                            result[15], #16
                            last_update_date, #17
                            last_update_date, #18
                            None)) #19
        elif action == 'change':
            datload.append((result[0], #1
                            result[1], #2
                            result[2], #3
                            result[3], #4
                            result[4], #5
                            result[5], #6
                            result[6], #7
                            result[7], #8
                            result[8], #9
                            formatDate(result[9]), #10
                            result[10], #11
                            result[11], #12
                            result[12], #13
                            result[13], #14
                            result[14], #15
                            result[15], #16
                            last_update_date, #17
                            formatDate(existingrecord[17]), #18
                            None)) #19

    if len(datload) > 0:
        cur.bindarraysize = len(datload)
        logger.info('Loading ' + str(len(datload)) + ' product rows to Product History Table')

        statement = """INSERT INTO {0} (INGREDIENT, DOSAGE_FORM, ROUTE, APPLICANT, STRENGTH, APPL_TYPE,
                    APPL_NO, PRODUCT_NO, TE_CODE, APPROVAL_DATE, RLD, RS, TYPE, APPLICANT_FULLNAME, REMARKS,
                    TRADE_NAME, LAST_UPDATE_DT, ADD_DT, REMOVE_DT)
                    VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9, TO_DATE(:10, 'dd-mon-yyyy'),
                    :11, :12, :13, :14, :15, :16, TO_DATE(:17, 'dd-mon-yyyy'), TO_DATE(:18, 'dd-mon-yyyy'),
                    TO_DATE(:19, 'dd-mon-yyyy'))"""
        try:
            cur.prepare(statement.format(tables['productshistory']))
            cur.executemany(None, datload)
            con.commit()
            logger.info(
                'Product records successfully loaded to Product History Table. ' + str(len(datload)) + ' rows added.')
        except cx_Oracle.DatabaseError as e:
            logger.error(e)
            logger.error('Database Error. Patent Load failed.')
    try:
        logger.info('Checking for Product deleted rows and updating REMOVE_DT')
        statement = """MERGE INTO {0} TEST
                         USING (SELECT T.INGREDIENT,
                                       T.APPL_NO,
                                       T.TYPE,
                                       T.PRODUCT_NO,
                                       T.REMOVE_DT,
                                       T.ADD_DT
                                  FROM {0} T
                                       LEFT OUTER JOIN
                                       (SELECT *
                                          FROM {1}
                                         WHERE LOAD_DT =
                                                  TO_DATE('{2}', 'dd-mon-yyyy'))
                                       D
                                          ON T.APPL_NO = D.APPL_NO
                                             AND T.TYPE = D.TYPE
                                             AND T.PRODUCT_NO = D.PRODUCT_NO
                                             AND T.INGREDIENT = D.INGREDIENT
                                 WHERE D.INGREDIENT IS NULL AND T.REMOVE_DT IS NULL) TEMP
                            ON (    TEMP.APPL_NO = TEST.APPL_NO
                                AND TEMP.TYPE = TEST.TYPE
                                AND TEMP.PRODUCT_NO = TEST.PRODUCT_NO
                                AND TEMP.INGREDIENT = TEST.INGREDIENT)
                    WHEN MATCHED
                    THEN
                       UPDATE SET TEST.REMOVE_DT = TO_DATE('{2}', 'dd-mon-yyyy')"""
        cur.execute(statement.format(tables['productshistory'], tables['productsdump'], last_update_date))
        logging.info('Products rows marked as removed: ' + str(cur.rowcount))
        con.commit()
    except cx_Oracle.DatabaseError as e:
        logger.error(e)
        logger.error('Database Error. REMOVE_DT record update failed.')


def loadExclusivityHistory(logger, cur, con, tables, last_update_date):
    logger.info('Beginning Exclusivity history table load.')
    # Read products CSV and iterate through rows
    logger.info('Retrieving Exclusivity Dump Table')
    select = """SELECT T.APPL_TYPE,
                    T.APPL_NO,
                    T.PRODUCT_NO,
                    T.EXCLUSIVITY_CODE,
                    T.EXCLUSIVITY_DATE,
                    COUNT(*)
                FROM {0} T
                WHERE T.LOAD_DT = TO_DATE('{1}', 'dd-mon-yyyy')
                GROUP BY T.APPL_TYPE,
                    T.APPL_NO,
                    T.PRODUCT_NO,
                    T.EXCLUSIVITY_CODE,
                    T.EXCLUSIVITY_DATE,
                    T.LOAD_DT"""
    cur.execute(select.format(tables['exclusivitydump'], last_update_date))
    results = cur.fetchall()
    datload = []

    for result in results:
        action = 'none'
        select = """SELECT *
                        FROM {0}
                        WHERE APPL_TYPE = '{1}'
                            AND APPL_NO = '{2}'
                            AND PRODUCT_NO = '{3}'
                            AND EXCLUSIVITY_CODE = '{4}'
                            AND EXCLUSIVITY_DATE = TO_DATE('{5}', 'dd-mon-yyyy')"""
        exclusivityDate = formatDate(result[4])
        cur.execute(select.format(tables['exclusivityhistory'],
                                  result[0], result[1], result[2], result[3], exclusivityDate))
        qresults = cur.fetchall()
        count = len(qresults)
        # There are no matching records in the DB, add a new record for this row
        if count == 0:
            logger.debug('New record: ' + str(result))
            action = 'add'
        # There are duplicate records. Strange. Do nothing. Investigate.
        elif count > 1:
            logger.error('Ambiguous matches in Exclusivity History Table: ' + str(result))
            c = 1
            for r in qresults:
                logger.error('Ambiguous match in HISTORY table ' + str(c) + ': ' + str(r))
                c += 1
        elif count == 1:
            existingrecord = qresults[0]
            if not result[5] == existingrecord[5]:
                logger.debug('Imperfect history dump record: ' + str(result))
                logger.debug('Imperfect history match, delete/readd: ' + str(existingrecord))
                action = 'change'
        if action == 'add':
            datload.append((result[0], #APPL_TYPE
                            result[1], #APPL_NO
                            result[2], #PRODUCT_NO
                            result[3], #EXCLUSIVITY_CODE
                            formatDate(result[4]), #EXCLUSIVITY_DATE
                            result[5], #EXCLUSIVITY_COUNT
                            last_update_date, #LAST_UPDATE_DT
                            last_update_date, #ADD_DT
                            None))  #REMOVE_DT
        elif action == 'change':
            # Delete old row
            delete = """DELETE FROM {0}
                                    WHERE APPL_TYPE = '{1}'
                            AND APPL_NO = '{2}'
                            AND PRODUCT_NO = '{3}'
                            AND EXCLUSIVITY_CODE = '{4}'
                            and EXCLUSIVITY_DATE = TO_DATE('{5}', 'dd-mon-yyyy')"""
            cur.execute(delete.format(tables['patentshistory'],
                                      existingrecord[0],
                                      existingrecord[1],
                                      existingrecord[2],
                                      existingrecord[3],
                                      existingrecord[4]))
            con.commit()
            exclusivityDate = formatDate(result[4])
            cur.execute(delete.format(result[0], result[1], result[2], result[3], exclusivityDate))
            # Add changed row to datload array
            datload.append((result[0], #APPL_TYPE
                            result[1], #APPL_NO
                            result[2], #PRODUCT_NO
                            result[3], #EXCLUSIVITY_CODE
                            formatDate(result[4]), #EXCLUSIVITY_DATE
                            result[5], #EXCLUSIVITY_COUNT
                            last_update_date, #LAST_UPDATE_DT
                            qresults[7], #ADD_DT
                            None))  #REMOVE_DT
    # Load into table
    if len(datload) > 0:
        cur.bindarraysize = len(datload)
        logger.info('Loading ' + str(len(datload)) + ' exclusivity rows to History Table')
        statement = """INSERT INTO {0} (APPL_TYPE, APPL_NO, PRODUCT_NO, EXCLUSIVITY_CODE,
                     EXCLUSIVITY_DATE, EXCLUSIVITY_COUNT, LAST_UPDATE_DT, ADD_DT, REMOVE_DT)
                    VALUES (:1, :2, :3, :4, TO_DATE(:5, 'dd-mon-yyyy'), :6,
                            TO_DATE(:7, 'dd-mon-yyyy'), TO_DATE(:8, 'dd-mon-yyyy'), TO_DATE(:9, 'dd-mon-yyyy'))"""
        cur.prepare(statement.format(tables['exclusivityhistory']))
        try:
            cur.executemany(None, datload)
            con.commit()
            logger.info('Exclusivity dates successfully loaded to History Table. ' + str(len(datload)) + ' rows added.')
        except cx_Oracle.DatabaseError as e:
            logger.error(e)
            logger.error('Database Error. Patent Load failed.')
    try:
        logger.info('Checking for exclusivity deleted rows and updating REMOVE_DT')
        statement = """MERGE INTO {0} TEST
                         USING (SELECT T.APPL_TYPE,
                                        T.APPL_NO,
                                        T.PRODUCT_NO,
                                        T.EXCLUSIVITY_CODE,
                                        T.EXCLUSIVITY_DATE
                                    FROM {0} T
                                       LEFT OUTER JOIN
                                       (SELECT *
                                          FROM {1}
                                         WHERE LOAD_DT =
                                                  TO_DATE('{2}', 'dd-mon-yyyy'))
                                       D
                                          ON T.PRODUCT_NO = D.PRODUCT_NO
                                             AND T.APPL_TYPE = D.APPL_TYPE
                                             AND T.EXCLUSIVITY_CODE = D.EXCLUSIVITY_CODE
                                             AND T.EXCLUSIVITY_DATE = D.EXCLUSIVITY_DATE
                                             AND T.APPL_NO = D.APPL_NO
                                 WHERE D.APPL_NO IS NULL AND T.REMOVE_DT IS NULL) TEMP
                            ON (    TEMP.APPL_NO = TEST.APPL_NO
                                AND TEMP.PRODUCT_NO = TEST.PRODUCT_NO
                                AND TEMP.EXCLUSIVITY_CODE = TEST.EXCLUSIVITY_CODE
                                AND TEMP.EXCLUSIVITY_DATE = TEST.EXCLUSIVITY_DATE)
                    WHEN MATCHED
                    THEN
                       UPDATE SET TEST.REMOVE_DT = TO_DATE('{2}', 'dd-mon-yyyy') WHERE TEST.REMOVE_DT IS NULL"""
        cur.execute(statement.format(tables['exclusivityhistory'], tables['exclusivitydump'] , last_update_date))
        logging.info('Exclusivity rows marked as removed: ' + str(cur.rowcount))
        con.commit()
    except cx_Oracle.DatabaseError as e:
        logger.error(e)
        logger.error('Database Error. REMOVE_DT record update failed.')


# This data is not clean enough to produce history table
def loadPatentHistory(logger, cur, con, tables):
    logger.info('Beginning Patent table load.')
    # Construct today's date for new records
    today = date.today()
    last_update_date = formatDate(today)
    last_update_date = '01-Feb-18'
    # Read products CSV and iterate through rows
    select = """SELECT T.APPL_TYPE,
                    T.APPL_NO,
                    T.PRODUCT_NO,
                    T.PATENT_NO,
                    T.PATENT_EXPIRE_DATE,
                    T.DRUG_SUBSTANCE_FLAG,
                    T.DRUG_PRODUCT_FLAG,
                    T.PATENT_USE_CODE,
                    T.DELIST_FLAG,
                    T.SUBMISSION_DT,
                    COUNT(*)
                FROM {0} T
                WHERE T.LOAD_DT = TO_DATE('{1}', 'dd-mon-yyyy')
                GROUP BY T.APPL_TYPE,
                    T.APPL_NO,
                    T.PRODUCT_NO,
                    T.PATENT_NO,
                    T.PATENT_EXPIRE_DATE,
                    T.DRUG_SUBSTANCE_FLAG,
                    T.DRUG_PRODUCT_FLAG,
                    T.PATENT_USE_CODE,
                    T.DELIST_FLAG,
                    T.SUBMISSION_DT"""
    logger.info('Retrieving Patent Dump Table')
    cur.execute(select.format(tables['patentsdump'], last_update_date))
    results = cur.fetchall()
    datload = []

    for result in results:
        action = 'none'
        # I don't think this is necessary anymore now that there's inline if statements to sub out "is Null" below
        # for i in result:
        #     if i is None:
        #         i = 'Null'
        select = """SELECT *
                        FROM {0}
                        WHERE APPL_TYPE = '{1}'
                            AND APPL_NO = '{2}'
                            AND PRODUCT_NO = '{3}'
                            AND PATENT_NO = '{4}'
                            AND PATENT_USE_CODE {5}"""
        cur.execute(select.format(tables['patentshistory'],
                                  result[0],
                                  result[1],
                                  result[2],
                                  result[3],
                                  "= '" + result[7] + "'" if result[7] is not None else "is Null"))
        qresults = cur.fetchall()
        count = len(qresults)
        # There are no matching records in the DB, add a new record for this row
        if count == 0:
            logger.debug('New record: ' + str(result))
            action = 'add'
        # There are duplicate records. Strange. Do nothing. Investigate.
        elif count > 1:
            logger.debug('Ambiguous matches in Patent History Table: ' + str(result))
            c = 1
            for r in qresults:
                logger.debug('Ambiguous match ' + str(c) + ': ' + str(r))
                c += 1
        elif count == 1:
            existingrecord = qresults[0]
            if not result[4] == existingrecord[4] and \
                result[5] == existingrecord[5] and \
                result[6] == existingrecord[6] and \
                result[7] == existingrecord[7] and \
                result[8] == existingrecord[8] and \
                result[9] == existingrecord[9] and \
                result[10] == existingrecord[10]:
                logger.debug('Imperfect match, delete record, readd: ' + str(result))
                action = 'change'
        if action == 'add':
            datload.append((result[0], #APPL_TYPE
                            result[1], #APPL_NO
                            result[2], #PRODUCT_NO
                            result[3], #PATENT_NO
                            formatDate(result[4]), #PATENT_EXPIRE_DATE
                            result[5], #DRUG_SUBSTANCE_FLAG
                            result[6], #DRUG_PRODUCT_FLAG
                            result[7], #PATENT_USE_CODE
                            result[8], #DELIST_FLAG
                            formatDate(result[9]), #SUBMISSION_DT
                            result[10], #PATENT_COUNT
                            last_update_date, #LAST_UPDATE_DT
                            last_update_date, #ADD_DT
                            None))  #REMOVE_DT
        elif action == 'change':
            # Delete old row
            delete = """DELETE FROM {0}
                        WHERE APPL_TYPE = '{1}'
                            AND APPL_NO = '{2}'
                            AND PRODUCT_NO = '{3}'
                            AND PATENT_NO = '{4}'
                            AND PATENT_USE_CODE {5}"""
            cur.execute(delete.format(tables['patentshistory'],
                  existingrecord[0],
                  existingrecord[1],
                  existingrecord[2],
                  existingrecord[3],
                  "= '" + existingrecord[7] + "'" if existingrecord[7] is not None else "is Null"))
            con.commit()
            #add revised row to datload array
            datload.append((result[0],  #APPL_TYPE
                            result[1],  #APPL_NO
                            result[2],  #PRODUCT_NO
                            result[3],  #PATENT_NO
                            formatDate(result[4]),  #PATENT_EXPIRE_DATE
                            result[5],  #DRUG_SUBSTANCE_FLAG
                            result[6],  #DRUG_PRODUCT_FLAG
                            result[7],  #PATENT_USE_CODE
                            result[8],  #DELIST_FLAG
                            formatDate(result[9]),  #SUBMISSION_DT
                            result[10],  # PATENT_COUNT
                            last_update_date,  #LAST_UPDATE_DT
                            formatDate(existingrecord[12]),  #ADD_DT
                            None))  #REMOVE_DT


    # Load into table
    if len(datload) > 0:
        cur.bindarraysize = len(datload)
        logger.info('Loading ' + str(len(datload)) + ' patent rows to history table')
        print(datload)
        statement = """INSERT INTO {0} (APPL_TYPE, APPL_NO, PRODUCT_NO, PATENT_NO,
                    PATENT_EXPIRE_DATE, DRUG_SUBSTANCE_FLAG, DRUG_PRODUCT_FLAG, PATENT_USE_CODE, DELIST_FLAG,
                    SUBMISSION_DT, PATENT_COUNT,
                    LAST_UPDATE_DT, ADD_DT, REMOVE_DT)
                    VALUES (:1, :2, :3, :4, TO_DATE(:5, 'dd-mon-yyyy'), :6,
                            :7, :8, :9, TO_DATE(:10, 'dd-mon-yyyy'), :11,
                            TO_DATE(:12, 'dd-mon-yyyy'), TO_DATE(:13, 'dd-mon-yyyy'), TO_DATE(:14, 'dd-mon-yyyy'))"""
        cur.prepare(statement.format(tables['patentshistory']))
        try:
            cur.executemany(None, datload)
            con.commit()
            logger.info('Patent records successfully loaded to History Table. ' + str(len(datload)) + ' rows added.')
        except cx_Oracle.DatabaseError as e:
            logger.error(e)
            logger.error('Database Error. Patent Load failed.')

    # try:
    #     logger.info('Checking for deleted rows and updating REMOVE_DT')
    #     statement = """MERGE INTO {0} TEST
    #                      USING (SELECT T.APPL_TYPE,
    #                                 T.APPL_NO,
    #                                 T.PRODUCT_NO,
    #                                 T.PATENT_NO,
    #                                 T.PATENT_USE_CODE
    #                               FROM {0} T
    #                                    LEFT OUTER JOIN
    #                                    (SELECT *
    #                                       FROM {1}
    #                                      WHERE LOAD_DT =
    #                                               TO_DATE('{2}', 'dd-mon-yyyy'))
    #                                    D
    #                                       ON     T.APPL_NO = D.APPL_NO
    #                                          AND T.APPL_TYPE = D.APPL_TYPE
    #                                          AND T.PRODUCT_NO = D.PRODUCT_NO
    #                                          AND T.PATENT_NO = D.PATENT_NO
    #                                          AND (T.PATENT_USE_CODE = D.PATENT_USE_CODE
    #                                             OR (COALESCE(T.PATENT_USE_CODE, D.PATENT_USE_CODE) is NULL))
    #                              WHERE D.PATENT_NO IS NULL) TEMP
    #                         ON (    TEST.APPL_NO = TEMP.APPL_NO
    #                                  AND TEST.APPL_TYPE = TEMP.APPL_TYPE
    #                                  AND TEST.PRODUCT_NO = TEMP.PRODUCT_NO
    #                                  AND TEST.PATENT_NO = TEMP.PATENT_NO
    #                                  AND (TEST.PATENT_USE_CODE = TEMP.PATENT_USE_CODE
    #                                     OR (COALESCE(TEST.PATENT_USE_CODE, TEMP.PATENT_USE_CODE) is NULL)))
    #                 WHEN MATCHED
    #                 THEN
    #                    UPDATE SET TEST.REMOVE_DT = TO_DATE('{2}', 'dd-mon-yyyy')"""
    #     cur.execute(statement.format(tables['patentshistory'], tables['patentsdump'], last_update_date))
    #     con.commit()
    # except cx_Oracle.DatabaseError as e:
    #     logger.error(e)
    #     logger.error('Database Error. REMOVE_DT record update failed.')



if __name__ == '__main__':
    url = 'https://www.fda.gov/downloads/Drugs/InformationOnDrugs/UCM163762.zip'
    data = 'C:/Users/cep/Documents/PycharmProjects/OBLoad/data'
    tables = {'productshistory':'T_OB_PRODUCTS_HISTORY_TEST',
              'patentshistory' : 'T_OB_PATENT_HISTORY_TEST',
              'exclusivityhistory': 'T_OB_EXCLUSIVITY_HISTORY_TEST',
              'productsdump': 'T_OB_PRODUCTS_DMP',
              'patentsdump': 'T_OB_PATENT_DMP_TEST',
              'exclusivitydump': 'T_OB_EXCLUSIVITY_DMP'}

    # logging instantiation
    logging.config.fileConfig('logging.ini')
    # create logger
    logger = logging.getLogger('OBRetrieve')
    # COMMENTED OUT TO TEST LOADING FUNCTIONS WITHOUT RETRIEVING ZIP repeatedly
    dir = "C:\\Users\\cep\\Documents\\PycharmProjects\\OBLoad\\data\\EOBZIP_2018_01"
    if dir is not None:
        con = cx_Oracle.connect('SCIOPS/SCIOPSd123@USP-ORADSD11.USP.ORG:1766/DSD11')
        cur = con.cursor()
        #loadProductsHistory(dir, logger, cur, con, tables['productshistory'])
        # TODO: Handle Logging for history loads. Note what's added/changed/removed in a separate log.
        # TODO: Check all queries for possibility of nulls, especially when where "="
        #loadExclusivityHistory(logger, cur, con, tables)
        loadPatentHistory(logger, cur, con, tables)