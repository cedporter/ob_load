###
# Dependency on Oracle Instant Client
###

import cx_Oracle
import logging
import logging.config
import OBDumpLoad
import OBHistoryLoad
import OBRetrieve
import configparser
from BufferingSMTPHandler import BufferingSMTPHandler
from datetime import date
from helper import formatDate

if __name__ == '__main__':

    config = configparser.ConfigParser()
    config.read('OB-config.ini')
    tables = config['tables']
    url = config['obdata']['url']
    data = config['obdata']['data']
    user = config['oracle']['user']
    ps = config['oracle']['pass']
    server = config['oracle']['server']
    port = config['oracle']['port']
    schema = config['oracle']['schema']
    emails = [e.strip() for e in config['email']['recipients'].split(',')]
    mailhost = config['email']['mailhost']


    # logging instantiation
    logging.config.fileConfig('logging.ini')
    # create logger
    logger = logging.getLogger('OBLog')
    dir = OBRetrieve.retrieveob(logger, data, url)
    if dir is not None:
        logger.info("Loading process started.")
        today = date.today()
        date = formatDate(today)
        con = cx_Oracle.connect(user + '/' + ps + '@' + server + ':' + port + '/' + schema)
        cur = con.cursor()
        OBDumpLoad.loadProductsDump(dir, logger, cur, con, tables, date)
        OBDumpLoad.loadExclusivityDump(dir, logger, cur, con, tables, date)
        OBDumpLoad.loadPatentsDump(dir, logger, cur, con, tables, date)
        OBHistoryLoad.loadProductsHistory(logger, cur, con, tables, date)
        OBHistoryLoad.loadExclusivityHistory(logger, cur, con, tables, date)
        #Patent table is not clean enough to build history table
        #OBHistoryLoad.loadPatentHistory(logger, cur, con, tables)
        logger.info("Loading process complete.")
        logging.shutdown()
