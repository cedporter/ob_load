import logging
import logging.config
import re
import csv
import cx_Oracle
import OBRetrieve
from helper import formatDate

def loadProductsDump(dir, logger, cur, con, tables, last_update_date):
    # Read products CSV and iterate through rows
    with open(dir + '/' + 'products.txt') as fin:
        reader = csv.DictReader(fin, delimiter="~")
        datload = []
        logger.info('Beginning Product table load.')
        for row in reader:
            # Data cleanup
            # Remove leading zeroes
            row['Product_No'] = row['Product_No'].lstrip('0')
            row['Appl_No'] = row['Appl_No'].lstrip('0')
            # escape single-quotes
            row['Ingredient'] = row['Ingredient'].replace('\'', '\'\'')
            #split dosage form and route
            DFcomb = row['DF;Route']
            spl = DFcomb.split(';')
            row['DF;Route'] = spl[0]
            row['Route'] = spl[1]
            # split note in strength and add to remarks
            row['Remarks'] = None
            if row['Strength'].find('**') == -1:
                row['Remarks'] = ''
            else:
                Scomb = row['Strength']
                spl = Scomb.split('**')
                row['Strength'] = spl[0].lstrip(' ')
                row['Remarks'] = spl[1]
            if row['Approval_Date'].find('Approved Prior') != -1:
                if row['Remarks'] != '':
                    row['Remarks'] += ', ' + row['Approval_Date']
                else:
                    row['Remarks'] = row['Approval_Date']
                row['Approval_Date'] = 'Dec 31, 1981'
            for r in row:
                if row[r] == '':
                    row[r] = None

            # Record build
            datload.append((row['Ingredient'], #1
                            row['DF;Route'], #2
                            row['Route'], #3
                            row['Applicant'], #4
                            row['Strength'], #5
                            row['Appl_Type'], #6
                            row['Appl_No'], #7
                            row['Product_No'], #8
                            row['TE_Code'], #9
                            row['Approval_Date'], #10
                            row['RLD'], #11
                            row['RS'], #12
                            row['Type'], #13
                            row['Applicant_Full_Name'], #14
                            row['Remarks'], #15
                            row['Trade_Name'], #16
                            last_update_date)) #17

        # Load into table
        if len(datload) > 0:
            cur.bindarraysize = len(datload)
            logger.info('Loading ' + str(len(datload)) + ' product rows')
            statement = """INSERT INTO {0} (INGREDIENT, DOSAGE_FORM, ROUTE, APPLICANT, STRENGTH,
                        APPL_TYPE, APPL_NO, PRODUCT_NO, TE_CODE, APPROVAL_DATE, RLD, RS, TYPE, APPLICANT_FULLNAME,
                        REMARKS, TRADE_NAME, LOAD_DT)
                        VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9, TO_DATE(:10, 'mon dd, yyyy'),
                        :11, :12, :13, :14, :15, :16, TO_DATE(:17, 'dd-mon-yyyy'))"""
            cur.prepare(statement.format(tables['productsdump']))
            try:
                cur.executemany(None, datload)
                con.commit()
                logger.info('Products successfully loaded. ' + str(len(datload)) + ' rows added.')
            except cx_Oracle.DatabaseError as e:
                logger.error(e)
                logger.error('Database Error. Product Load failed.')


def loadPatentsDump(dir, logger, cur, con, tables, last_update_date):
    # Read products CSV and iterate through rows
    with open(dir + '/' + 'patent.txt') as fin:
        reader = csv.DictReader(fin, delimiter="~")
        datload = []
        logger.info('Beginning Patent table load')
        for row in reader:
            # Data cleanup
            # Remove leading zeroes
            row['Product_No'] = row['Product_No'].lstrip('0')
            row['Appl_No'] = row['Appl_No'].lstrip('0')
            for r in row:
                if row[r] == '':
                    row[r] = None

            # Record build
            datload.append((row['Appl_Type'], #1
                            row['Appl_No'], #2
                            row['Product_No'], #3
                            row['Patent_No'], #4
                            row['Patent_Expire_Date_Text'], #5
                            row['Drug_Substance_Flag'], #6
                            row['Drug_Product_Flag'], #7
                            row['Patent_Use_Code'], #8
                            row['Delist_Flag'], #9
                            row['Submission_Date'], #10
                            last_update_date)) #11

        # Load into table
        if len(datload) > 0:
            cur.bindarraysize = len(datload)
            logger.info('Loading ' + str(len(datload)) + ' patent rows')
            statement = """INSERT INTO {0} (APPL_TYPE, APPL_NO, PRODUCT_NO, PATENT_NO,
                         PATENT_EXPIRE_DATE, DRUG_SUBSTANCE_FLAG, DRUG_PRODUCT_FLAG, PATENT_USE_CODE,
                         DELIST_FLAG, SUBMISSION_DT, LOAD_DT)
                        VALUES (:1, :2, :3, :4, TO_DATE(:5, 'mon dd, yyyy'),
                                :6, :7, :8, :9, TO_DATE(:10, 'mon dd, yyyy'), TO_DATE(:11, 'dd-mon-yyyy'))"""
            cur.prepare(statement.format(tables['patentsdump']))
            try:
                cur.executemany(None, datload)
                con.commit()
                logger.info('Patents successfully loaded. ' + str(len(datload)) + ' rows added.')
            except cx_Oracle.DatabaseError as e:
                logger.error(e)
                logger.error('Database Error. Patent Load failed.')


def loadExclusivityDump(dir, logger, cur, con, tables, last_update_date):
    # Read products CSV and iterate through rows
    with open(dir + '/' + 'exclusivity.txt') as fin:
        reader = csv.DictReader(fin, delimiter="~")
        datload = []
        logger.info('Beginning Exclusivity table load.')
        for row in reader:
            # Data cleanup
            # Remove leading zeroes
            row['Product_No'] = row['Product_No'].lstrip('0')
            row['Appl_No'] = row['Appl_No'].lstrip('0')
            for r in row:
                if row[r] == '':
                    row[r] = None

            # Record build
            datload.append((row['Appl_Type'], #1
                            row['Appl_No'], #2
                            row['Product_No'], #3
                            row['Exclusivity_Code'], #4
                            row['Exclusivity_Date'], #5
                            last_update_date)) #6

        # Load into table
        if len(datload) > 0:
            cur.bindarraysize = len(datload)
            logger.info('Loading ' + str(len(datload)) + ' exclusivity rows')
            statement = """INSERT INTO {0} (APPL_TYPE, APPL_NO, PRODUCT_NO, EXCLUSIVITY_CODE,
                         EXCLUSIVITY_DATE, LOAD_DT)
                        VALUES (:1, :2, :3, :4, TO_DATE(:5, 'mon dd, yyyy'),
                                TO_DATE(:6, 'dd-mon-yyyy'))"""
            cur.prepare(statement.format(tables['exclusivitydump']))
            try:
                cur.executemany(None, datload)
                con.commit()
                logger.info('Exclusivity dates successfully loaded. ' + str(len(datload)) + ' rows added.')
            except cx_Oracle.DatabaseError as e:
                logger.error(e)
                logger.error('Database Error. Patent Load failed.')


if __name__ == '__main__':
    url = 'https://www.fda.gov/downloads/Drugs/InformationOnDrugs/UCM163762.zip'
    data = 'C:/Users/cep/Documents/PycharmProjects/OBLoad/data'
    tables = {'productshistory':'T_OB_PRODUCTS_HISTORY_TEST',
              'patentshistory' : 'T_OB_PATENTS_HISTORY_TEST',
              'exclusivityhistory': 'T_OB_EXCLUSIVITY_HISTORY_TEST',
              'productsdumptable': 'T_OB_PRODUCTS_DMP',
              'patentsdumptable': 'T_OB_PATENTS_DMP',
              'exclusivitydumptable': 'T_OBD_EXCLUSIVITY_DMP'}

    # logging instantiation
    logging.config.fileConfig('logging.ini')
    # create logger
    logger = logging.getLogger('OBRetrieve')
    dir = OBRetrieve.retrieveob(logger, data, url)
    if dir is not None:
        con = cx_Oracle.connect('SCIOPS/SCIOPSd123@USP-ORADSD11.USP.ORG:1766/DSD11')
        cur = con.cursor()
        loadProductsDump(dir, logger, cur, con, tables)
        #loadExclusivityDump(dir, logger, cur, con, tables)
        #loadPatents(dir, logger, cur, con, tables)